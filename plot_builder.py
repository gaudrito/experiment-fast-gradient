#!/usr/bin/env python

import sys, math, re

g_error = 0
g_count = 0
g_errlow = 0
g_cntlow = 0

# rounds a float represented as string
def rounder(f):
    f = round(float(f),2)
    return repr(int(f) if f == int(f) else f)

# check if float is number
def isnum(f):
    return not (math.isnan(f) or math.isinf(f))

# average of a list of floats
def avg(l):
    if len(set(l))==1:
        return l[0]
    lshort = [f for f in l if isnum(f)]
    return sum(lshort)/float(len(lshort)) if len(lshort)>0 else float("nan")

# component-wise average of list of vectors
def average(data):
    global g_error, g_errlow, g_count, g_cntlow
    
    if len(data) == 1:
        return data[0]
    l = len(data[0])
    El = tuple(avg([d[i] for d in data]) for i in range(l))
    Vl = tuple(avg([d[i]**2 for d in data]) for i in range(l))
    Vl = tuple(math.sqrt(max(Vl[i]/(El[i]**2)-1,0)) for i in range(l) if abs(El[i])>0 and Vl[i] < float("inf"))
    g_count += len(Vl)
    g_error += sum(Vl)
    if len(Vl) > 1:
        g_cntlow += len(Vl)-1
        g_errlow += sum(v-Vl[0] for v in Vl[1:] if v > Vl[0])
    return El

# aggregate list of vector into num buckets
def bucketize(data, num):
    data.sort()
    l = len(data)/num
    return [average(data[l*i:l*(i+1)]) for i in range(num)]

# grep field names
def get_headers(path, type):
    if type == "space":
        return [t.split('@')[0] for t in open(path).readlines()[6].split()[2:] if t != "#"]
    elif type == "time":
        return [t.split('[')[0] for t in open(path).readlines()[6].split() if t != "#"]

# prints plotstyles of sufficient length
def get_plotstyle(n):
    symb = ["cross", "box", "asterisk", "soliddiamond", "point", "circle", "diagonalcross", "solidcircle", "diamond", "solidbox"] * (1+n/10)
    style = ["solid", "dash", "dot", "dashdot", "longdash", "spacedash", "spacedot"] * (1+n/7)
    style = ["solid", "dash", "dashdot", "spacedash"] * (1+n/4)
    return '[' + ', '.join(style[:n]) + ']'

# parse file contents into list of vectors
def parse_file(path, type, n):
    if type == "space":
        data = [float(x) for x in open(path).readlines()[8].split()[1:]]
        l = len(data)/n
        return [tuple(data[j*n+i] for j in range(l)) for i in range(n)]
    elif type == "time":
        return [[float(x) for x in r.split()] for r in open(path).readlines()[8:-3]]

# parse files contents into list of vectors
def parse_files(paths, type, n):
    data = []
    for p in paths:
        data += parse_file(p, type, n)
    return data

# print vectors into separate plots by component
def separate_print(data, headers, title):
    datas = {}
    plots = []
    for i in headers:
        if len(i.split('-')) > 1:
            s = i.split('-')[-1]
            if not s in plots:
                plots.append(s)
                datas[s] = ([], [])
    for i in range(len(data[0])):
        hh = headers[i].split('-')
        if len(hh) > 1:
            s = hh[-1]
            t = hh[0] if len(hh)==2 else ("%s (%s)" % (hh[0],hh[1]) if len(hh)==3 else ' '.join(hh[:-1]))
            datas[s][1].append(t)
            datas[s][0].append([[d[0], d[i]] for d in data])
    print "# %s" % title
    print "P := Array(1..%d):" % (len(plots))
    i=1
    for p in plots:
        print "P[%d] := plot(%s, legend=%s, linestyle=%s, labels=%s):" % (i, repr(datas[p][0]), re.sub("'", '"', repr(datas[p][1])), get_plotstyle(len(datas[p][1])), repr([headers[0],p]))
        i=i+1
    print "display(P%s);" % (", title = "+repr(title) if len(title)>0 else "")


if __name__ == "__main__":
    type = sys.argv[1]
    if type == "space":
        bnum = int(sys.argv[2])
        devnum = int(sys.argv[3])
        files = sys.argv[4:]
    elif type == "time":
        files = sys.argv[2:]
        bnum = len(open(files[0]).readlines()[8:-3])
        devnum = 1

    title = list(set.intersection(*[set(re.sub(r'([0-9]|^[a-z/]*)_', r'\1#', f[:-4]).split('#')[1:]) for f in files]))
    title.sort()
    title = ' and '.join([p.split('-')[0]+'='+rounder(p.split('-')[1]) for p in title])

    print >> sys.stderr, "Parsing %d files%s by %s into %d buckets (%d device clusters)..." % (len(files), " with "+title if len(title) > 0 else "", type, bnum, devnum),

    separate_print(bucketize(parse_files(files, type, devnum), bnum), get_headers(files[0], type), title)

    print >> sys.stderr, "done: %.2f%%/%.2f%% error." % (100*g_error/max(g_count,1), 100*g_errlow/max(g_cntlow,1))
