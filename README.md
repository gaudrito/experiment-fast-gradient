# Optimally-Self-Healing Distributed Gradient Structures through Bounded Information Speed

Submitted to [COORDINATION (DisCoTec 2017)](http://2017.discotec.org/). For any issues with reproducing the experiments, please contact [Giorgio Audrito](mailto:giorgio.audrito@unito.it).

## Description

This repository contains all the source code used to perform the experiments presented in the paper.

## Prerequisites

* Importing the repository
    - Git

* Executing the simulations
    - Java 8 JDK update 112 or above installed and set as default virtual machine
    - Eclipse Mars or above (suggested)

* Chrunching the generated data and producing the charts
    - Python 3.6.0 or above installed and set as default Python interpreter
    - Maple 18 or above
  
**Notes about the supported operating systems**

All the tests have been performed under Linux, no test has been performed under any other operating system, and we only support reproducing the tests under that environment.
Due to the portable nature of the used tools, the test suite should run also on other Unix-like OSs (e.g. BSD, MacOS X).

## Reproducing the experiment

### Importing the repository

The first step is cloning this repository.
It can be easily done by using `git`.
Open a terminal, move it into an empty folder of your choice, then issue the following command:

``git clone https://gaudrito@bitbucket.org/gaudrito/experiment-fast-gradient.git``

This should make all the required files appear in your repository, with the latest version.

### Inspecting the code and simulation GUI 

Open Eclipse, click on "File > Import" and then on "Gradle > Gradle Project", then select the folder of the repository donwloaded in the previous step.

To properly visualize the source files you should also install the Protelis Parser through "Help > Install New Software" with the following address:

``http://efesto.apice.unibo.it/protelis-build/protelis-parser/protelis.parser.repository/target/repository/``

The files with the source code are in `src/main/protelis/` and contain the following:

* `utils.pt`: utility functions
* `gradients.pt`: code of all gradient algorithms
* `performance.pt`: code of the performance comparison (as in Section 4.2)
* `study.pt`: code of the case study (as in Section 4.3)

The files describing the environment are in `src/main/yaml/` and contain the following:

* `performance.yaml`: environment used for the performance comparison (Section 4.2)
* `study.yaml`: environment used for the case study (Section 4.3)

In order to run the simulations with a GUI, create a Run Configuration in Eclipse with the following settings.

* Main class: `it.unibo.alchemist.Alchemist`
* Program arguments: `-g src/main/resources/effects.aes -y src/main/yaml/X.yml` where `X` can be either `performance` or `study` 

### Executing the simulations

In order to re-execute the simulations, compiled java class files (from `src/main/java/`) and a copy of all files in directory `src/main/protelis/` need to be present in directory `bin/`. If you already run the simulations with Eclipse (as in the point above), they should already be present. Otherwise, you can produce them with:

``./batch_tester.sh --compile``

issued from the root directory where the repository was cloned. The whole set of simulations can be executed by issuing the following commands:

``./batch_tester.sh study 300 30000 algo random``
``./batch_tester.sh performance 10 1000 space_variability time_or_noise random``

You can track the progress of the simulations through the file `nohup.out`. Results will be written in directory `data/raw/`.

### Producing the graphs

The raw files can be converted into plottable text through:

``./batch_tester.sh --plot study "Case Study" algo classic crf flex bis``
``./batch_tester.sh --plot performance "Performance Comparison"``

Results will be written in the `data/` directory into text files containing Maple source code able to produce the corresponding plots.
